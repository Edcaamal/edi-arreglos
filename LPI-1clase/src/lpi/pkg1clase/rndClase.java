/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lpi.pkg1clase;

import java.util.Random;


/**
 *
 * @author edgar
 */
public class rndClase {
    public static String almVisual(){
        int    totAlumnos = 32; 
        double semAlumno;
        int    idAlumno;
        
        String[]  aAlumnosLPA;
        aAlumnosLPA    = new String[totAlumnos];
        aAlumnosLPA[0]= "Adrian E Aguilar Quej";   
        aAlumnosLPA[1]= "Carlos A Alvarez Queb";   
        aAlumnosLPA[2]= "Javier E Balmes Castillo";   
        aAlumnosLPA[3]= "Denilson Blanco Garcia";   
        aAlumnosLPA[4]= "Mauricio A Blanquet Rodriguez";   
        aAlumnosLPA[5]= "Katia G Calan Chi";   
        aAlumnosLPA[6]= "Andre Calderon Martinez";   
        aAlumnosLPA[7]= "Karla D Carrillo Arcos";   
        aAlumnosLPA[8]= "Carolina Chi Arceo";   
        aAlumnosLPA[9]= "William J Chi Rico";   
        aAlumnosLPA[10]= "Samuel J Dzec Lopez";   
        aAlumnosLPA[11]= "Miguel A Dzib Alayola";   
        aAlumnosLPA[12]= "Maria J Gonzalez Barahona";   
        aAlumnosLPA[13]= "Johnny A Heredia Reveles";   
        aAlumnosLPA[14]= "Jossmar O Ku Cupul";   
        aAlumnosLPA[15]= "Alonso Medina Reyes";   
        aAlumnosLPA[16]= "Victor M Mora Alvarez";   
        aAlumnosLPA[17]= "Eduardo A Morales Espinosa";   
        aAlumnosLPA[18]= "Rafael A Moreno Chel";   
        aAlumnosLPA[19]= "Rey A Paz Carrillo";   
        aAlumnosLPA[20]= "Geder N Perez Dzib";   
        aAlumnosLPA[21]= "Luis R Perez Martinez";   
        aAlumnosLPA[22]= "Miguel A Rivas Gomez";   
        aAlumnosLPA[23]= "Adriana L Rosado Uicab";   
        aAlumnosLPA[24]= "Oscar A Ruiz Velazquez";   
        aAlumnosLPA[25]= "Arely S Salazar Uc";   
        aAlumnosLPA[26]= "Michael A Salvador Cocom";   
        aAlumnosLPA[27]= "Jorge A Sulub Xool";   
        aAlumnosLPA[28]= "Salvador M Valle Saenz";   
        aAlumnosLPA[29]= "Victor B Vazquez Mejia";   
        aAlumnosLPA[30]= "Santiago J Vela Mena";   
        aAlumnosLPA[31]= "Jesus D Zapata Leon";       
        
        Random rndAlumnos = new Random();
        
        semAlumno = ((rndAlumnos.nextDouble()*totAlumnos));
        idAlumno  = (int)(semAlumno);
        return idAlumno+ " - " +aAlumnosLPA[idAlumno];
    }
    
    public static void main(String[] args) {
        // System.out.println(almVisual());    
        // 22 - Miguel A Rivas Gomez
        int totAlumnos = 6;
        String[]  aAlumnosLPA;
        aAlumnosLPA    = new String[totAlumnos];
        aAlumnosLPA[0]= "Adrian E Aguilar Quej";   
        aAlumnosLPA[1]= "Carlos A Alvarez Queb";   
        aAlumnosLPA[2]= "Javier E Balmes Castillo";   
        aAlumnosLPA[3]= "Denilson Blanco Garcia";   
        aAlumnosLPA[4]= "Mauricio A Blanquet Rodriguez";   
        aAlumnosLPA[5]= "Katia G Calan Chi";   

        int[]  matricula;
        matricula    = new int[3];
        matricula[0]= 10;   
        matricula[1]= 11;   
        matricula[2]= 12;   

        
        for (int i = 0; i < aAlumnosLPA.length; i++) {
            System.out.println(i);
            System.out.println(aAlumnosLPA[i]);
        }
        
        for (String alumnos : aAlumnosLPA) {
            System.out.println(alumnos);
            
        }
        for (int mat : matricula) {
            System.out.println(mat);
        }
        
        
        // Ciclico decremental 
        int x = 11;
        while (x <= 10){
            System.out.println("Valor de x: " + x);
            x++;
        }
        
        int y = 11;
        do {
            System.out.println("Valor de -y- : " + y);
            y++;
        } while (y <= 10);

        
        
        // Ciclo con condición Booleana
        boolean valor = true;
        x = 1;
        while (valor){
            System.out.println("Valor de x: " + x);
            x++;
            if (x == 30){
                valor = false;
             }
        }

        // Ciclo con condicion booleana II
        valor = true;
        x = 1;
        int suma = 0;
        while (valor)
        {
            System.out.println("Valor de x: " + x+ "  = "+suma );
             x++;
             suma += x;  
            if (suma >= 30){
                valor = false;
                
            }
        }
        
        String[][]  aPersonajes;
        aPersonajes  = new String[16][3];
        aPersonajes[0][0] = "Luke Skywalker";
        aPersonajes[0][1] = "172";
        aPersonajes[0][2] = "male";  
        aPersonajes[1][0] = "R2-D2";
        aPersonajes[1][1] = "96";
        aPersonajes[1][2] = "n/a";  
        aPersonajes[2][0] = "C-3PO";
        aPersonajes[2][1] = "167";
        aPersonajes[2][2] = "n/a";  
        aPersonajes[3][0] = "Darth Vader";
        aPersonajes[3][1] = "202";
        aPersonajes[3][2] = "male";  
        aPersonajes[4][0] = "Leia Organa";
        aPersonajes[4][1] = "150";
        aPersonajes[4][2] = "female";  
        aPersonajes[5][0] = "Owen Lars";
        aPersonajes[5][1] = "178";
        aPersonajes[5][2] = "male";  
        aPersonajes[6][0] = "Beru Whitesun lars";
        aPersonajes[6][1] = "165";
        aPersonajes[6][2] = "female";  
        aPersonajes[7][0] = "R5-D4";
        aPersonajes[7][1] = "97";
        aPersonajes[7][2] = "n/a";  
        aPersonajes[8][0] = "Biggs Darklighter";
        aPersonajes[8][1] = "183";
        aPersonajes[8][2] = "male";  
        aPersonajes[9][0] = "Obi-Wan Kenobi";
        aPersonajes[9][1] = "182";
        aPersonajes[9][2] = "male";  
        aPersonajes[10][0] = "Yoda";
        aPersonajes[10][1] = "66";
        aPersonajes[10][2] = "male";  
        aPersonajes[11][0] = "Jek Tono Porkins";
        aPersonajes[11][1] = "180";
        aPersonajes[11][2] = "male";  
        aPersonajes[12][0] = "Jabba Desilijic Tiure";
        aPersonajes[12][1] = "175";
        aPersonajes[12][2] = "hermaphrodite";  
        aPersonajes[13][0] = "Han Solo";
        aPersonajes[13][1] = "180";
        aPersonajes[13][2] = "male";  
        aPersonajes[14][0] = "Chewbacca";
        aPersonajes[14][1] = "228";
        aPersonajes[14][2] = "male";  
        aPersonajes[15][0] = "Anakin Skywalker";
        aPersonajes[15][1] = "188";
        aPersonajes[15][2] = "male"; 
        
        System.out.println("-----for i anidado dinamico 1");
        for (int i = 0; i < aPersonajes.length; i++) {
            String[] aPersonaje = aPersonajes[i];
            for (int j = 0; j < aPersonaje.length; j++) {
                String personaje = aPersonaje[j];
                System.out.println(personaje);
            }
        }
        
        
        System.out.println("-----for i anidado dinamico 2");
        for (int i = 0; i < aPersonajes.length; i++) {
            String[] aPersonaje = aPersonajes[i];
            for (int j = 0; j < aPersonaje.length; j++) {
                System.out.println(aPersonaje[j]);   
            }        
        }
        System.out.println("-----for i fijo");        
        for (int i = 0; i < aPersonajes.length; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.println(aPersonajes[i][j]);   
            }        
        }
        System.out.println("-----for each");
        
        for (String[] aPersonaje : aPersonajes) {
            for (String personaje : aPersonaje) {
                System.out.println(personaje);              
            }          
        }
    }
    
}
